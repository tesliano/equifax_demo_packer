#Simple creacion de imagen con packer en google cloud.

Dentro de image_create 
   * packer build -var-file=equifaxdemoimage/variables.json equifax_demo.json

Dentro de instance_create 
   * terraform apply -auto-approve  

Borrar imagen 
   * echo "Y" | gcloud compute images delete  equifaxdemoimage

Borrar instancia
   * terraform destroy -auto-approve 
