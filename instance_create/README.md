Simple instance on GCP with containerized rundeck

gcloud auth application-default login

gcloud compute ssh INSTANCE-NAME

gcloud compute instances list

gcloud compute ssh equifax-rundeck-poc -- 'sudo cat /root/rundeck_password'

gcloud compute project-info describe --project project-id
