// Configure the Google Cloud provider and the region CANADA
provider "google" {
 credentials = file("/home/mdipietro/.config/gcloud/application_default_credentials.json")
 project     = "equifax-demo-258920"
 region      = "${var.region}"
}