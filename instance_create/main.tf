// A single Google Cloud Engine instance with 8 vcpu and 16gb of ram
resource "google_compute_instance" "default" {
  name = "${var.company}-${var.project}-${var.env}"
  machine_type = "n1-standard-4"
  zone = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "equifaxdemoimage"
    }
  }

   network_interface {
      network = "default"
      access_config {
        //For dynamic IP.
        }
      }

}

// A variable for extracting the external ip of the instance
output "instance_ip" {
 value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}
