//define basic firewall rules.
resource "google_compute_firewall" "default" {
    name = "${var.company}-${var.project}-${var.env}"
    network = "default"

    allow {
      protocol = "icmp"
    }

    allow {
      protocol = "tcp"
      ports = ["22", "80"]
    }

    allow {
      protocol = "udp"
      ports = ["22", "80"]
    }

}
