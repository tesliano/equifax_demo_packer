//Simple vars file
variable "project" {
        default = "packer"
    }
variable "env" {
        default = "poc"
    }
variable "company" {
        default = "equifax"
    }
variable "zone" {
        default = "northamerica-northeast1-c"
    }
variable "region" {
        default = "northamerica-northeast1"
    }
